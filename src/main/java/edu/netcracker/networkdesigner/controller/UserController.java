package edu.netcracker.networkdesigner.controller;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.model.dto.UserDTO;
import edu.netcracker.networkdesigner.service.database.ProjectService;
import edu.netcracker.networkdesigner.service.database.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectService projectService;

    @PostMapping()
    public User addUser(@RequestBody UserDTO dto) {
        return userService.addUser(dto);
    }

    @GetMapping("/{userId}")
    public User getUserById(@PathVariable(value = "userId") Long id) {
        return userService.getUserById(id);
    }

    @PatchMapping("/{userId}")
    public User updateUser(@PathVariable(value = "userId") Long id,
                           @RequestBody UserDTO userDTO) {
        return userService.updateUser(id, userDTO);
    }

    @GetMapping("/projects")
    public List<Project> getAvailableProjectsByUser(@AuthenticationPrincipal User user) {
        return getAvailableProjectsByUserId(user.getId());
    }

    @GetMapping("{userId}/projects")
    public List<Project> getAvailableProjectsByUserId(@PathVariable(name = "userId") Long userId) {
        return projectService.getAvailableProjectsByUserId(userId);
    }

    @GetMapping()
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }
}
