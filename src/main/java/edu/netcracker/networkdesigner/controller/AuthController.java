package edu.netcracker.networkdesigner.controller;

import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.model.dto.UserDTO;
import edu.netcracker.networkdesigner.payload.ApiResponse;
import edu.netcracker.networkdesigner.payload.JwtAuthenticationResponse;
import edu.netcracker.networkdesigner.payload.LoginRequest;
import edu.netcracker.networkdesigner.security.JwtTokenProvider;
import edu.netcracker.networkdesigner.service.common.AuthenticationService;
import edu.netcracker.networkdesigner.service.database.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.UUID;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private JwtTokenProvider tokenProvider;

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        return authenticationService.authenticate(loginRequest);
    }

    @PostMapping("/signup")
    public ResponseEntity<ApiResponse> registerUser(@Valid @RequestBody UserDTO signUpRequest) {
        return authenticationService.registration(signUpRequest);
    }

    @GetMapping("/activation/{uuid}")
    public ResponseEntity<ApiResponse> activation(@PathVariable UUID uuid) throws UserPrincipalNotFoundException {
        return userService.activate(uuid);
    }

    @GetMapping("/current/{token}")
    public User getCurrentUser(@PathVariable String token) {
        return userService.getUserById(tokenProvider.getUserIdFromJWT(token));
    }

    @PostMapping("/validate")
    public boolean validateToken(@RequestBody String token) {
        JSONObject tokenJson = new JSONObject(token);
        return tokenProvider.validateToken(tokenJson.get("value").toString());
    }
}
