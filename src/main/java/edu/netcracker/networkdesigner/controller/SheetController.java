package edu.netcracker.networkdesigner.controller;

import edu.netcracker.networkdesigner.database.entity.Sheet;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;
import edu.netcracker.networkdesigner.service.database.ProjectService;
import edu.netcracker.networkdesigner.service.database.SheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sheets")
public class SheetController {

    @Autowired
    private SheetService sheetService;

    @GetMapping()
    public List<Sheet> getAllSheets(){
        return sheetService.getAllSheets();
    }

    @PatchMapping("/{sheetId}")
    public Sheet updateSheet(@PathVariable(value = "sheetId") Long sheetId,
                             @RequestBody SheetDTO dto){
        return sheetService.updateSheet(dto, sheetId);
    }

    @DeleteMapping("/{sheetId}")
    public void deleteSheet(@PathVariable("sheetId") Long sheetId){
        sheetService.deleteSheet(sheetId);
    }
}
