package edu.netcracker.networkdesigner.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
public class SheetDTO {

    private String name;

    public SheetDTO() {
        this.name = "DEFAULT SHEET NAME";
    }
}
