package edu.netcracker.networkdesigner.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum ProjectStatus {
    APPROVED("approved"),
    DRAFT("draft"),
    ARCHIVED("archived");
    private final String state;

    ProjectStatus(String state) {
        this.state = state;
    }

    @JsonValue
    public String getState() {
        return state;
    }
}
