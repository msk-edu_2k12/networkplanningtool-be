package edu.netcracker.networkdesigner.service.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.bson.Document;
import org.springframework.stereotype.Service;

@Service
public interface StorageService {

    String getDocumentByIdAndCollection(String collectionName, String id) throws JsonProcessingException;

    String saveDocument(String collectionName, Document body);

    String getAllDocuments(String collectionName) throws JsonProcessingException;

    void deleteDocumentByIdAndCollection(String collectionName, String id);

    String updateDocumentByIdAndCollection(String collectionName, String id, Document body) throws JsonProcessingException;
}
