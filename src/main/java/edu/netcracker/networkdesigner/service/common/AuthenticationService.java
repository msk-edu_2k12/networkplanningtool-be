package edu.netcracker.networkdesigner.service.common;

import edu.netcracker.networkdesigner.model.dto.UserDTO;
import edu.netcracker.networkdesigner.payload.ApiResponse;
import edu.netcracker.networkdesigner.payload.JwtAuthenticationResponse;
import edu.netcracker.networkdesigner.payload.LoginRequest;
import org.springframework.http.ResponseEntity;

public interface AuthenticationService {

    ResponseEntity<JwtAuthenticationResponse> authenticate(LoginRequest loginRequest);

    ResponseEntity<ApiResponse> registration(UserDTO signUpRequest);
}
