package edu.netcracker.networkdesigner.service.common.impl;

import edu.netcracker.networkdesigner.database.entity.ModelInfo;
import edu.netcracker.networkdesigner.enums.ModelType;
import edu.netcracker.networkdesigner.exception.ModelNotFoundException;
import edu.netcracker.networkdesigner.exception.StorageException;
import edu.netcracker.networkdesigner.exception.StorageFileNotFoundException;
import edu.netcracker.networkdesigner.parser.YangModelParser;
import edu.netcracker.networkdesigner.parser.YangUtils;
import edu.netcracker.networkdesigner.service.database.ModelService;
import edu.netcracker.networkdesigner.service.common.YangStorageService;
import edu.netcracker.networkdesigner.utils.Utils;
import org.opendaylight.yangtools.yang.model.api.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class YangStorageServiceImpl implements YangStorageService {

    @Value("${app.storage.location}")
    private String storageLocation;

    @Autowired
    private ModelService modelService;

    @Override
    public Long store(MultipartFile file, ModelType type) throws StorageException {
        ModelInfo info = new ModelInfo(file, type);
        String nameWithExtension = StringUtils.cleanPath(file.getOriginalFilename());
        String fileName = Utils.getNameWithTimeStamp(nameWithExtension, info.getCreatedAt());

        Path path = Paths.get(storageLocation);
        if (file.isEmpty()) {
            throw new StorageException("Failed to store empty file " + fileName);
        }
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, path.resolve(fileName),
                    StandardCopyOption.REPLACE_EXISTING);
        }
        catch (IOException e) {
            throw new StorageException("Failed to store file " + fileName, e);
        }

        YangModelParser yangModelParser = new YangModelParser(Paths.get(path.toString(), fileName));
        Module module = yangModelParser.findModuleByName(
                com.google.common.io.Files.getNameWithoutExtension(nameWithExtension));
        info.setJsonRepresentation(
                YangUtils.getJsonRepresentationForModule(module).toString());

        modelService.addInfo(info);
        return info.getId();
    }

    @Override
    public Resource load(Long infoId) throws ModelNotFoundException {
        Resource resource = getYangFile(infoId);
        if (!(resource.exists() || !resource.isReadable())) {
            throw new StorageFileNotFoundException(
                    "Could not read file: " + resource.getFilename());
        }
        return resource;
    }

    @Override
    public String getContent(Long infoId) throws ModelNotFoundException, IOException {
        Resource resource = getYangFile(infoId);
        return new String(
                Files.readAllBytes(Paths.get(resource.getURI())));
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(Paths.get(storageLocation));
        }
        catch (Exception e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }

    private Resource getYangFile(Long infoId) throws ModelNotFoundException {
        ModelInfo info = modelService.getModelInfoById(infoId);
        String fileName = Utils.getNameWithTimeStamp(info.getFileName(), info.getCreatedAt());
        Resource resource;
        Path file = Paths.get(storageLocation).resolve(fileName);
        try {
            resource = new UrlResource(file.toUri());
        } catch (MalformedURLException e) {
            throw new StorageException("Exception while take URL for loaded file", e);
        }
        return resource;
    }
}
