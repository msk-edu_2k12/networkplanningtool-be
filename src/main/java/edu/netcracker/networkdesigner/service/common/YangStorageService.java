package edu.netcracker.networkdesigner.service.common;

import edu.netcracker.networkdesigner.enums.ModelType;
import edu.netcracker.networkdesigner.exception.ModelNotFoundException;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface YangStorageService {

    Long store(MultipartFile file, ModelType type);

    void init();

    Resource load(Long infoId) throws ModelNotFoundException;

    String getContent(Long infoId) throws ModelNotFoundException, IOException;
}
