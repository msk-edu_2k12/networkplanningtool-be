package edu.netcracker.networkdesigner.service.common.impl;

import edu.netcracker.networkdesigner.database.repository.ModelInfoRepository;
import edu.netcracker.networkdesigner.enums.ModelType;
import edu.netcracker.networkdesigner.exception.ModelNotFoundException;
import edu.netcracker.networkdesigner.service.common.YangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class YangServiceImpl implements YangService {

    @Autowired
    private ModelInfoRepository modelInfoRepository;

    @Override
    public String getJsonYangModelByType(String type) throws ModelNotFoundException {
        return modelInfoRepository
                .findFirstByTypeOrderByCreatedAtDesc(ModelType.convert(type)).orElseThrow(
                    () -> new ModelNotFoundException("ModelInfo object with type=(" + type + ") was not found"))
                .getJsonRepresentation();
    }
}
