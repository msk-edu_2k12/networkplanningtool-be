package edu.netcracker.networkdesigner.service.database.impl;

import edu.netcracker.networkdesigner.database.entity.Project;
import edu.netcracker.networkdesigner.database.entity.Sheet;
import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.database.repository.ProjectRepository;
import edu.netcracker.networkdesigner.database.repository.UserRepository;
import edu.netcracker.networkdesigner.enums.ProjectType;
import edu.netcracker.networkdesigner.exception.AppException;
import edu.netcracker.networkdesigner.exception.ResourceNotFoundException;
import edu.netcracker.networkdesigner.model.dto.ProjectDTO;
import edu.netcracker.networkdesigner.model.dto.SheetDTO;
import edu.netcracker.networkdesigner.service.database.ProjectService;
import edu.netcracker.networkdesigner.service.database.SheetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SheetService sheetService;

    @Override
    public Project getProjectById(Long id) {
        return projectRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Project",
                        "id", String.valueOf(id)));
    }

    @Override
    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    @Override
    public Project addProject(ProjectDTO projectDTO, User owner) {
        Project project = new Project(projectDTO, owner);
        Project result = projectRepository.save(project);
        sheetService.addSheet(new SheetDTO(), project);
        return result;
    }

    @Override
    public Project addProject(ProjectDTO projectDTO, Long ownerId) {
        User user = userRepository.findById(ownerId)
                .orElseThrow(() -> new ResourceNotFoundException("User",
                        "id", String.valueOf(ownerId)));
        return addProject(projectDTO, user);
    }

    @Override
    public void deleteProject(Long id) {
        Project project = getProjectById(id);
        for (Sheet sheet: project.getSheets()) {
            sheetService.deleteSheet(sheet.getId());
        }
        projectRepository.delete(project);
    }

    @Override
    public Project addPrivateUser(Long userId, Long projectId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User",
                        "id", String.valueOf(userId)));
        Project currentProject = getProjectById(projectId);
        if (!currentProject.getMembers().contains(user)) {
            currentProject.getMembers().add(user);
        }
        return projectRepository.save(currentProject);
    }

    @Override
    public List<User> getProjectMembers(Long projectId) {
        Project project = getProjectById(projectId);
        return project.getType().equals(ProjectType.PRIVATE)
                ? project.getMembers()
                : userRepository.findAll();
    }

    @Override
    @Transactional
    public List<Project> getAvailableProjectsByUserId(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User",
                        "id", String.valueOf(userId)));
        return getAvailableProjectsByUser(user);
    }

    @Override
    public List<Project> getAvailableProjectsByUser(User user) {
        List<Project> projects = new ArrayList<>();
        projects.addAll(user.getProjects());
        projects.addAll(user.getPersonalProjects());
        projects.addAll(projectRepository.getProjectsByType(ProjectType.PUBLIC));
        return projects.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public Project updateProject(Long projectId, ProjectDTO projectDTO) {
        Project project = getProjectById(projectId);
        project.setName(projectDTO.getName());
        project.setType(projectDTO.getType());
        project.setDescription(projectDTO.getDescription());
        return projectRepository.save(project);
    }

    @Override
    public Project addSheet(SheetDTO sheetDTO, Long projectId) {
        sheetService.addSheet(sheetDTO, getProjectById(projectId));
        return getProjectById(projectId);
    }

    @Override
    public Project deleteMember(Long projectId, Long userId) {
        Project project = projectRepository.findById(projectId).orElseThrow(
                () ->  new AppException("Project with id = " + projectId + " not found")
        );
        User user = userRepository.findById(userId).orElseThrow(
                () ->  new AppException("User with id = " + userId + " not found")
        );
        project.getMembers().remove(user);
        return projectRepository.save(project);
    }
}
