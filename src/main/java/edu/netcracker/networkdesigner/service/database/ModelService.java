package edu.netcracker.networkdesigner.service.database;

import edu.netcracker.networkdesigner.database.entity.ModelInfo;
import edu.netcracker.networkdesigner.exception.ModelNotFoundException;

import java.util.List;

public interface ModelService {

    ModelInfo addInfo(ModelInfo modelInfo);

    List<ModelInfo> getAll();

    ModelInfo getModelInfoById(Long infoId) throws ModelNotFoundException;
}
