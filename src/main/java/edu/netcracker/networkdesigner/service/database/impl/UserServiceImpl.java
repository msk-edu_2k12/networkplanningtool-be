package edu.netcracker.networkdesigner.service.database.impl;

import edu.netcracker.networkdesigner.database.entity.User;
import edu.netcracker.networkdesigner.database.repository.UserRepository;
import edu.netcracker.networkdesigner.model.dto.UserDTO;
import edu.netcracker.networkdesigner.payload.ApiResponse;
import edu.netcracker.networkdesigner.service.database.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.nio.file.attribute.UserPrincipalNotFoundException;
import java.util.List;
import java.util.UUID;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String usernameOrEmail)
            throws UsernameNotFoundException {
        return userRepository.findByUsernameOrEmail(usernameOrEmail, usernameOrEmail)
                .orElseThrow(() ->
                        new UsernameNotFoundException("User not found with username or email : " + usernameOrEmail)
                );
    }

    @Override
    public ResponseEntity<ApiResponse> activate(UUID uuid) throws UserPrincipalNotFoundException {
        User userData = userRepository.findByActivationCode(uuid).orElseThrow(
                () -> new UserPrincipalNotFoundException("User by activation code not found")
        );
        userData.setActive(true);
        userData.setActivationCode(null);
        userRepository.save(userData);
        return new ResponseEntity<>(new ApiResponse(true, "Email has been confirmed."), HttpStatus.OK);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public User getUserById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    @Override
    public User updateUser(Long id, UserDTO userDTO) {
        User user = getUserById(id);
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());
        user.setEmail(userDTO.getEmail());
        user.setUsername(userDTO.getUsername());
        user.setPhone(userDTO.getPhone());
        return userRepository.save(user);
    }

    @Override
    public User addUser(UserDTO dto) {
        User user = new User(dto);
        userRepository.save(user);
        return user;
    }
}
