package edu.netcracker.networkdesigner.exception;

public class ModelNotFoundException extends Exception{

    public ModelNotFoundException(String message) { super(message); }

    public ModelNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
