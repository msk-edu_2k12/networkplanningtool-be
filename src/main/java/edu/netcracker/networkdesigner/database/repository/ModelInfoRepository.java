package edu.netcracker.networkdesigner.database.repository;

import edu.netcracker.networkdesigner.database.entity.ModelInfo;
import edu.netcracker.networkdesigner.enums.ModelType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ModelInfoRepository extends JpaRepository<ModelInfo, Long> {

    List<ModelInfo> findAll();

    Optional<ModelInfo> findById(Long infoId);
    
    Optional<ModelInfo> findFirstByTypeOrderByCreatedAtDesc(ModelType type);
}
